import numpy as np
from firstSignChange import firstSignChange
from typing import Callable


def secant(f: Callable, x0: float, x1: float, maxIter: int = 100, epsilon: float=0.000001) -> float:
    """
    Find the root of a function using the secant method.

    Args:
        f (Callable): A callable function representing the equation for which the root is to be found.
        x0 (float): The first initial guess for the root.
        x1 (float): The second initial guess for the root.
        maxIter (int, optional): The maximum number of iterations (default is 100).
        epsilon (float, optional): Tolerance for convergence (default is 0.000001).

    Returns:
        float: An approximate value of the root of the equation.

    Raises:
        ValueError: If the root is not found within the specified number of iterations.

    """
    
    for i in range(maxIter):
        fx0 = f(x0)
        fx1 = f(x1)
        
        if abs(fx1) < epsilon:
            return x1
        
        x2 = x1 - (fx1 * (x1 - x0)) / (fx1 - fx0)
        x0, x1 = x1, x2
        
    raise ValueError("Root not found by secant method")
    
    
if __name__ == "__main__":
    poly = np.poly1d([1, 4, 3, 2])
    
    # Imagine that the value was found. For another function, you need to change xmin and xmax
    x0, x1 = firstSignChange(poly, -10, 10, 2)
    
    print(f"x0 = {x0:.1f}")
    print(f"f(x):\n{poly}")
    print(f"x = {secant(poly, x0, x1)}")
    