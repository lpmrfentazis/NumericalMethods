import numpy as np
from typing import Callable, Tuple


def firstSignChange(f: Callable, xmin: float = -30, xmax: float = 30, step: float = 2) -> Tuple[float, float]:
    """
    Find the first sign change in a function within a given range using a specified step.

    Args:
        f (Callable): A callable function representing the equation for which the sign change is to be found.
        xmin (float, optional): The minimum value of x within the search range (default is -30).
        xmax (float, optional): The maximum value of x within the search range (default is 30).
        step (float, optional): The step size for iterating through x values (default is 2).

    Returns:
        tuple or None: A tuple containing the x values where the sign change occurs, or None if no sign change is found.
        
    Raises:
        ValueError: If the interval is not found within the specified params.

    Notes:
        This function searches for the first sign change in the function f(x) within the specified range [xmin, xmax]
        using a step size of 'step'. If a sign change is found between f(x1) and f(x2), a tuple (x1, x2) is returned.
        If no sign change is found, the function returns None.
    """
    
    xlist = np.linspace(xmin, xmax, (xmax-xmin)//step)
    
    for i in range(1, len(xlist)):
        
        x1, x2 = xlist[i - 1], xlist[i]
        y1, y2 = f(x1), f(x2)
        
        if y1 * y2 < 0:
            return x1, x2
        
    raise ValueError("Interval not found by firstSingChange. Try change xmin, xmax, step")