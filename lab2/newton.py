import numpy as np
from firstSignChange import firstSignChange


def newton(f: np.poly1d, x0: float, maxIter: int = 100, epsilon: float=0.000001) -> float:
    """
    Calculate the root of an equation using the Newton-Raphson method.

    Args:
        x0 (float): The initial guess for the root.
        f (np.poly1d): The equation in the form of an np.poly1d object for which the root is to be determined.
        maxIter (int, optional): The maximum number of iterations (default is 100).
        epsilon (float, optional): Tolerance for convergence (default is 0.000001).

    Returns:
        float: An approximate value of the root of the equation.
    
    Raises:
        ValueError: If the root is not found within the specified number of iterations.
    """
    
    der = np.polyder(f)
    
    x = x0 - f(x0) / der(x0)
    
    for i in range(maxIter):
        x0 = x
        x = x0 - f(x0)/der(x0)

        if abs(x - x0) < epsilon:
            return x
        
    raise ValueError("Root not found by Newton method")
    
    
    
if __name__ == "__main__":
    poly = np.poly1d([1, 4, 3, 2])
    
    # For another function, you need to change xmin and xmax
    x0, _ = firstSignChange(poly, -10, 10, 2)
    
    print(f"x0 = {x0:.1f}")
    print(f"f(x):\n{poly}")
    print(f"x = {newton(poly, x0)}")
    
    
    