import numpy as np
from firstSignChange import firstSignChange
from typing import Callable


def simpleNewton(f: Callable, x0: float, x1: float, maxIter: int = 100, epsilon: float=0.000001) -> float:
    """
    Calculate the root of an equation using the Newton-Raphson method.

    Args:
        x0 (float): The initial guess for the root.
        x1 (float): The second initial guess for the root.
        f (np.poly1d): The equation in the form of an np.poly1d object for which the root is to be determined.
        maxIter (int, optional): The maximum number of iterations (default is 100).
        epsilon (float, optional): Tolerance for convergence (default is 0.000001).

    Returns:
        float: An approximate value of the root of the equation.

    Raises:
        ValueError: If the root is not found within the specified number of iterations.
        
    """
    
    derApr = lambda x0, x1: (f(x1) - f(x0))/(x1 - x0)
    
    x = x1 - f(x1) / derApr(x0, x1)

    for i in range(maxIter):
        x0, x1 = x1, x
        
        x = x1 - f(x1) / derApr(x0, x1)

        if abs(x - x0) < epsilon:
            return x
        
    raise ValueError("Root not found by simpleNewton method")
    
    
if __name__ == "__main__":
    poly = np.poly1d([1, 4, 3, 2])
    
    # Imagine that the value was found. For another function, you need to change xmin and xmax
    x0, x1 = firstSignChange(poly, -10, 10, 2)
    
    print(f"x0 = {x0:.1f}")
    print(f"f(x):\n{poly}")
    print(f"x = {simpleNewton(poly, x0, x1)}")
    