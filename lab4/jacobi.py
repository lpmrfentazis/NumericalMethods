import numpy as np
from typing import Tuple


def jacobi(a: np.ndarray, maxIterations: int = 1000, epsilon: float = 1e-6) -> Tuple[np.ndarray, np.ndarray]:
    n = a.shape[0]
    p = np.eye(n)
    
    for _ in range(maxIterations):
        maxOffDiag = 0
        
        row, col = 0, 0
        
        for i in range(n):
            for j in range(i + 1, n):
                if abs(a[i, j]) > maxOffDiag:
                    maxOffDiag = abs(a[i, j])
                    row, col = i, j
                    
        
        if maxOffDiag < epsilon:
            break
            
        if a[row, row]  == a[col, col]:
            angle = np.pi/4
        else:
            angle = np.arctan(2 * a[row, col] / (a[row, row] - a[col, col])) / 2
            
        r = np.eye(n)
        r[row, row] = np.cos(angle)
        r[col, col] = np.cos(angle)
        r[row, col] = -np.sin(angle)
        r[col, row] = np.sin(angle)

        a = r.T.dot(a).dot(r)
        p = p.dot(r)
        
        eigenvalues = a.diagonal()
        
        return eigenvalues, p
    

if __name__ == "__main__":
    """
        Метод вращения (найти все векторы и собств. знач)
    """
    
    a = np.array([[2, 5, 0],
                  [1, -3, 0],
                  [0, 6, -2]])
    
    eigenvalues, eigenvector = jacobi(a)
    
    print(f"Eigenvalues: \n{eigenvalues}")
    print(f"Eigenvector: \n{eigenvector}")