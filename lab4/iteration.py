import numpy as np
from typing import Tuple


def iteration(a: np.ndarray, maxIterations: int = 1000, epsilon: float = 1e-6) -> Tuple[float, np.ndarray]:
    n = a.shape[0]
    x = np.random.rand(n)
    
    for _ in range(maxIterations):
        ax = a.dot(x)
        tmp = ax / np.linalg.norm(ax)
        
        delta = np.linalg.norm(tmp - x)
        x = tmp
        
        if delta <= epsilon:
            break
        
    eigenvalue = x.dot(ax) / x.dot(x)
    
    return eigenvalue, x


if __name__ == "__main__":
    """
        Метод итераций (найти только максимальное по модулю собств. знач и соотв вектор)
    """
    
    a = np.array([[2, 5, 0],
                  [1, -3, 0],
                  [0, 6, -2]])
    
    maxEigenvalue, maxEigenvector = iteration(a)
    
    print(f"Eigenvalue: {maxEigenvalue}")
    print(f"Eigenvector: \n{maxEigenvector}")