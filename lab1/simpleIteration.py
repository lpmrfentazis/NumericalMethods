from typing import Callable

def simpleIteration(x0: float, phi: Callable, epsilon: float = 0.0001, maxSteps: int = 100) -> float | None:
    """
    Approximates a root of an equation using the simple iteration method.

    Args:
        x0 (float): The initial guess for the root.
        phi (Callable): A callable function representing the iterative function phi(x).
        epsilon (float, optional): The desired level of accuracy for the root approximation (default is 0.0001).
        maxSteps (int, optional): The maximum number of iterations (default is 100).

    Returns:
        float or None: An approximate root of the equation if found within the specified accuracy and iteration limit,
        or None if the method did not converge.

    Notes:
        The simple iteration method, also known as fixed-point iteration, is used to find an approximate root of the equation
        f(x) = 0 by iteratively applying the function phi(x) until convergence or reaching the maximum number of iterations.
        If phi(x) == x, the method may not converge.

    """
    x = x0
    dx = float("inf")
    
    if phi(x) == 0:
        return x
    
    for i in range(maxSteps):
        xLast = x
        x = phi(xLast)
        dx = x - xLast
        
        if abs(dx) < epsilon:
            break
    
    if x == x0:
        return None
    
    return x


if __name__ == "__main__":
    
    x0 = 0
    f = lambda x: x**2 - 3*x - 4
    phi = lambda x: (x**2 - 4)/3
    
    x = simpleIteration(x0, phi)
    
    print(f"x0 = {x0}")
    print("f(x) = x^2 - 3x - 4")
    print("phi(x) = (x^2 - 4) / 3")
    print(f"x = {x}, f(x) = {f(x)}")
    