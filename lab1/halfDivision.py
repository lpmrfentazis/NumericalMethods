from typing import Callable



def halfDivision(f: Callable, x0: float, x1: float, epsilon: float = 0.0001, maxSteps: int = 100) -> float | None:    
    """
    Implements the bisection method (half division) to find the root of the equation f(x) = 0.

    Parameters:
        f (Callable): The function for which the root is being searched (must be a callable function that takes a single argument).
        x0 (float): Initial approximation (left boundary of the interval).
        x1 (float): Initial approximation (right boundary of the interval).
        epsilon (float, optional): The error at which the algorithm stops executing (default is 0.0001).
        maxSteps (int, optional): Maximum number of iterations (default is 100).

    Returns:
        float or None: The root of the equation f(x) = 0 found by the bisection method. Otherwise, None if the method does not converge.
    """
    
    if f(x0) == 0:
        return x0
    
    elif f(x1) == 0:
        return x1
    
    x = None 
    
    for i in range(maxSteps):
        x = (x0 + x1) / 2
        
        if (x1 - x0) >= epsilon: 
            if f(x0) * f(x) < 0:
                x1 = x
            else:
                x0 = x
        else:
            break
    
    return x
    
    
if __name__ == "__main__":
    f = lambda x: x**2 + x - 21
    
    x = halfDivision(f, -100, 100)
    
    print("f(x) = x^2 + x - 21")
    print(f"x = {x}, f(x) = {f(x)}")