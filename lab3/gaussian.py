import numpy as np



def gaussian(matrix: np.array, consts: np.array) -> np.array:
    """
    Solves a system of linear equations using Gaussian method.

    Args:
        matrix (np.array): The coefficient matrix of the system of equations.
        consts (np.array): The vector of constants on the right-hand side of the equations.

    Returns:
        np.array: The solution vector containing the values of the unknowns.

    """
    
    n = len(matrix)
    
    # Conversion to step matrix
    for row in range(n):
        rowWithMax = row
        
        for i in range(row+1, n):
            if abs(matrix[i, row]) > abs(matrix[rowWithMax, row]):
                rowWithMax = i
                
        matrix[[row, rowWithMax]] = matrix[[rowWithMax, row]]
        consts[[row, rowWithMax]] = consts[[rowWithMax, row]]
        
        mainElem = matrix[row, row]
        
        if mainElem == 0:
            continue
        
        matrix[row, :] /= mainElem
        consts[row] /= mainElem
        
        for i in range(row + 1, n):
            k = matrix[i, row]
            
            matrix[i, :] -= k * matrix[row, :]
            consts[i] -= k * consts[row]
            
    
    x = np.zeros(n)
    
    for i in range(n-1, -1, -1):
        x[i] = consts[i]
        
        for j in range(i + 1, n):
            x[i] -= matrix[i, j] * x[j]
            
    return x


if __name__ == "__main__":
    matrix = np.array([[1, 1, 2],
                       [2, 2, 4],
                       [3, 3, 6]], dtype=np.float32)
    
    consts = np.array([1, 2, 3], dtype=np.float32)
    
    print(f"matrix: \n{matrix}\n")
    
    print(f"consts: {consts}\n")
    
    x = gaussian(matrix, consts)
    
    print("x =", x)