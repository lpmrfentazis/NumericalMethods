from typing import Iterable, Optional

def parabolicInterpolation(x: Iterable[float], y: Iterable[float], target: float) -> Optional[float]:
    n = len(x)
    
    for i in range(n - 2):
        if x[i] <= target <= x[i + 2]:
            h = x[i + 1] - x[i]
            t = (target - x[i]) / h
            
            return (1 - t) * ((1 - t) * y[i] + t * y[i + 1]) + t* ((1 - t) * y[i + 1] + t * y[i + 2])


if __name__ == "__main__":
    """
        Параболическая интерполяция (две усредненных параболы)
    """
    x = [1, 4, 3, 2]
    y = [2, 3, 1, 4]
    target = 2.2

    result = parabolicInterpolation(x, y, target)
    print(f"x: {target:.2f}, y: {result:.2f}")