from typing import Iterable

def globalInterpolation(x: Iterable[float], y: Iterable[float], target: float) -> float:
    result = 0
    n = len(x)
    
    for i in range(n):
        term = y[i]
        
        for j in range(n):
            if j != i:
                term *= (target - x[j]) / (x[i] - x[j])
                
        result += term

    return result


if __name__ == "__main__":
    """
        Глобальный способ
    """
    x = [1, 4, 3, 2]
    y = [2, 3, 1, 4]
    target = 2.2

    result = globalInterpolation(x, y, target)
    print(f"x: {target:.2f}, y: {result:.2f}")