from typing import Iterable, Optional

def linearInterpolation(x: Iterable[float], y: Iterable[float], target: float) -> Optional[float]:
    n = len(x)
    
    for i in range(n - 1):
        if x[i] <= target <= x[i + 1]:
            slope = (y[i + 1] - y[i]) / (x[i + 1] - x[i])
            
            return y[i] + slope * (target - x[i])


if __name__ == "__main__":
    """
        Линейная интерполяция
    """
    x = [1, 4, 3, 2]
    y = [2, 3, 1, 4]
    target = 2.2

    result = linearInterpolation(x, y, target)
    print(f"x: {target:.2f}, y: {result:.2f}")