import numpy as np


def dividedDifference(x: np.ndarray, y: np.ndarray) -> np.ndarray:
    n = len(y)
    table = np.zeros((n, n))
    table[:, 0] = y

    for j in range(1, n):
        for i in range(n - j):
            table[i, j] = (table[i + 1, j - 1] - table[i, j - 1]) / (x[i + j] - x[i])

    return table[0, :]

def interpolation(x: np.ndarray, y: np.ndarray, xi: float) -> float:
    coef = dividedDifference(x, y)
    result = coef[0]

    for i in range(1, len(coef)):
        term = coef[i]
        
        for j in range(i):
            term *= (xi - x[j])
            
        result += term

    return result


if __name__ == "__main__":
    """
        Используя данные значения функции f(х) в таблице и методы численных расчетов, необходимо:
            Вычислить значение интерполяционного полинома Ньютона N3 (х) для неравноотстоящих узлов.
            Вычислить значение интерполяционного полинома Ньютона N3 (х) для равноотстоящих узлов.
            Вычислить значение интерполяционного полинома Ньютона N3(х) для узлов, 
                выбранных по какому-либо конкретному правилу (не указано на фото).
            Определить, чему равно значение функции f(х) в точке х* = 1.5, используя интерполяционный полином Ньютона N4(х), 
                если известно, что f(4) = 0, и сформировать соответствующий многочлен.
    """
    x = np.array([0, 1, 2, 3, 4])
    y = np.array([1, 2, 4, 3, 1])
    
    n3Irregular = interpolation(x, y, 1.5)
    n3EvenlySpaced = interpolation(np.linspace(min(x), max(x), len(x)), y, 1.5)
    n3Custom = interpolation(np.arange(2, 12, 2), y, 1.5)
    
    xN4 = np.append(x, 4)
    yN4 = np.append(y, 0)
    
    n4 = interpolation(xN4, yN4, 1.5)

    # Вывести результаты
    print(f"1. {n3Irregular}")
    print(f"2. {n3EvenlySpaced}")
    print(f"3. {n3Custom}")
    print(f"4. {n4}")
    